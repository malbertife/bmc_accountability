:-module(ics,[ics/2]).

ics([h(action(B,bid(Q)),Tbid),h(action(A,answ(B,win,Q)),Twin),h(action(A,deliver),Tdel)],
        [[Tpay<Tdel+3,e(action(B,pay(Q)),Tpay)]]).

ics([h(action(A,opauc),Topen),h(action(B,bid(Q)),Tbid),h(action(A,answ(B,win,Q)),Twin)],
        [[Tdel<Twin+5,e(action(A,deliver),Tdel)]]).

ics([h(action(Auc,answ(Bwin,win,Qwin)),Twin)],
        [[en(action(Auc,answ(Blose,win,Qlose)),Tlose),not_unif(Bwin,Blose)]]).

ics([h(action(Bidder1,bid(Q1)),T1)],
        [[T2>T1,Q2=<Q1,en(action(Bidder2,bid(Q2)),T2)]]).

ics([h(action(Auc,opauc),Topen),h(action(Bidder1,bid(Q1)),T1)],
        [[Q2>Q1,T2<T1+2,e(action(Bidder2,bid(Q2)),T2)],
        [e(action(Auc,answ(Bidder1,win,Q1)),Twin),Twin<T1+2]]).

ics([h(action(Auc,opauc),Topen),h(action(B,opauc),TB)],
        [[unif(Auc,B),unif(TB,Topen)]]).

ics([h(action(Auc,opauc),Topen)],
        [[unif(Auc,a)]]).

ics([h(action(B,bid(Q)),Topen)],
        [[not_unif(B,a)]]).

