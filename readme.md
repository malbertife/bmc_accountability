This repository contains the Prolog and SCIFF code for the examples in the paper

M.Gavanelli, M.Alberti and E.Lamma, "Accountable Protocols in Abductive Logic Programming", ACM Transactions on Internet Technology 18(4):1-20.

Prerequisites
---------
A recent stable version of SWI Prolog (http://www.swi-prolog.org). Tested on Windows 10 and Linux Debian 8.

The ```lambda``` pack for SWI Prolog should be installed (by the ```pack_install(lambda).``` query).

Running the examples in a UNIX-style terminal
---------

1. Clone this repository (```git clone https://bitbucket.org/malbertife/bmc_accountability```) or download it from the [Downloads section](https://bitbucket.org/malbertife/bmc_accountability/downloads/) and unpack it.
2. ```cd``` to the ```sciff``` folder.
3. Run SWI-Prolog
4. Consult the ```sciff.pl``` file (for example issuing ```[sciff].``` to the SWI-Prolog prompt).
5. Run the query 

``` bmc_acc(Protocol,LAgents,Verbosity,Timeout,Results).```

where

- ```Protocol``` is the protocol name (```fpsbAuctionOriginal``` for the original First Price Sealed Bid protocol, ```fpsbAuctionAccountable``` for the revised (accountable) First Price Sealed Bid protocol, ```englishAuction``` for the English Auction protocol)
- ```LAgents``` is a ground list of agents (such as ```[alice,bob]```)
- ```Verbosity``` is the verbosity level (```0``` is fine if you are only interested in the results)
- ```Timeout``` is the time in milliseconds allowed for the computation (such as ```100000``` for 100 seconds)
- ```Results``` should be left unbound and will be unified with a list of terms, each in either of the following formats:
    - ```ok(Depth,Time)``` meaning that the tool proved in ```Time``` milliseconds that the protocol is accountable up to depth ```Depth```
    - ```timeout(Time)``` meaning that the tool timed out at ```Time``` milliseconds
    - ```events(History,Time)``` meaning that the protocol found in ```Time``` milliseconds ```History``` (a list of H atoms) a counterexample to the accountability of the protocol

For example, the query
 
```bmc_acc(fpsbAuctionAccountable,[a,b],0,1000,Results).```

should produce a unification like

```Results = [timeout(1000),ok(2,614),ok(1,67),ok(0,3)]```

meaning that the tool found the protocol accountable for depths ```0```, ```1``` and ```2``` (in ```3```, ```67```, and ```614``` milliseconds, respectively), and then timed out at ```1000``` milliseconds.

Writing your own protocols
-----------------
Follow the instructions on the [SCIFF user manual](http://lia.deis.unibo.it/research/sciff/userman.html).