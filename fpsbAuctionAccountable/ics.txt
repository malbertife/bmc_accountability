H(action(B,bid(Q)),Tbid) /\
H(action(A,answ(B,win,Q)),Twin) /\
H(action(A,deliver),Tdel)
---> E(action(B,pay(Q)),Tpay) /\ Tpay <Tdel + 3.

H(action(A,opauc), Topen) /\
H(action(B,bid(Q)),Tbid) /\
H(action(A,answ(B,win,Q)),Twin)
---> E(action(A,deliver),Tdel) /\ Tdel < Twin + 5.

H(action(Auc,answ(Bwin,win,Qwin)), Twin)
---> EN(action(Auc,answ(Blose,win,Qlose)), Tlose) /\ Bwin != Blose.

% QUESTA E` SOLO PER LA ENGLISH AUCTION
%H(action(Bidder1, bid(Q1) ), Tbid)
%---> EN(action(Bidder2, bid(Q2) ), T2) /\ T2 > T1 /\ Q2 <= Q1.

H(action(Auc,opauc), Topen) /\
H(action(Bidder1,bid(Q1)), T1) /\ T1<10
---> E(action(Bidder2,bid(Q2)), T2) /\ Q2>Q1 /\ T2<10
\/   E(action(Auc,answ(Bidder1,win,Q1)), Twin) /\ Twin < T1 +2.

% IC che istituisce l'accountability
H(action(Auc,opauc), Topen) /\ H(action(Bidder1,bid(Q1) ),Tbid) --->  E(action(Auc,answ(Bidder2,win,Q2)), Twin).

H(action(Auc,opauc), Topen) /\
H(action(B,opauc), TB) ---> Auc=B /\ TB=Topen.

H(action(Auc,opauc), Topen) ---> Auc = a.
H(action(B,bid(Q)), Topen) ---> B != a.
